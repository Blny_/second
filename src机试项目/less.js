module.exports = {
  install: function (less, pluginManager, functions) {
    functions.add("vw", function (param) {
      return `calc(${param.value}/375 * 100vw)`;
    });
  },
};
